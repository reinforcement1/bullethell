import arcade
import math
from helpers import EntityCollisionsHelper
from player import Player

MAX_SPEED = 8
ACCELERATION = 0.5


class Bullet(arcade.Sprite):
    def __init__(self, scale: float, center_x: float, center_y: float):
        super().__init__("sprites/ennemy_projectile_blue_1.png", scale)

        self.center_x = center_x
        self.center_y = center_y

        self.speed = 0

        self.is_moving_up = False
        self.is_moving_down = False
        self.is_moving_left = False
        self.is_moving_right = False

        self.is_accelerating = False
        self.is_decelerating = False

    def handle_movements(self):
        if self.is_decelerating:
            self.speed -= ACCELERATION
            if self.speed <= 0:
                self.speed = 0
                self.is_decelerating = False

        if self.is_accelerating:
            self.speed += ACCELERATION
            if self.speed >= MAX_SPEED:
                self.speed = MAX_SPEED
                self.is_accelerating = False

        if self.is_moving_up:
            self.center_y += self.speed
        if self.is_moving_down:
            self.center_y -= self.speed
        if self.is_moving_left:
            self.center_x -= self.speed
        if self.is_moving_right:
            self.center_x += self.speed

    def on_update(self, delta_time: float = 1 / 60):
        self.handle_movements()
        if EntityCollisionsHelper.is_out_of_bounds(self):
            self.remove_from_sprite_lists()


class TargetedBullet(Bullet):
    def __init__(self, scale: float, center_x: float, center_y: float, target_x: float, target_y: float):
        super().__init__(scale, center_x, center_y)

        self.target_x = target_x
        self.target_y = target_y
        self.is_accelerating = True

        self.angle = math.atan2(target_y - center_y, target_x - center_x)

    def handle_movements(self):
        if self.is_accelerating:
            self.speed += ACCELERATION
            if self.speed >= MAX_SPEED:
                self.speed = MAX_SPEED
                self.is_accelerating = False

        self.center_x += math.cos(self.angle) * self.speed
        self.center_y += math.sin(self.angle) * self.speed

    def on_update(self, delta_time: float = 1 / 60):
        super().on_update(delta_time)


class BossBullet(Bullet):
    def __init__(
        self,
        scale: float,
        center_x: float,
        center_y: float,
        target_x: float,
        target_y: float,
        player: Player,
        stop_time: float,
        target_time: float,
    ):
        super().__init__(scale, center_x, center_y)
        self.target_x = target_x
        self.target_y = target_y
        self.player = player
        self.stop_time = stop_time
        self.target_time = target_time
        self.is_accelerating = True
        self.total_time = 0.0
        self.phase = 1

        self.angle = math.atan2(target_y - center_y, target_x - center_x)

    def handle_movements(self):
        self.center_x += math.cos(self.angle) * self.speed
        self.center_y += math.sin(self.angle) * self.speed

    def on_update(self, delta_time: float = 1 / 60):
        if EntityCollisionsHelper.is_out_of_bounds(self):
            self.remove_from_sprite_lists()
        self.total_time += delta_time

        if self.is_accelerating:
            self.speed += ACCELERATION * 3.4

        if self.total_time >= self.stop_time and self.phase == 1:
            self.phase += 1
            self.is_accelerating = False
            self.speed = 0.0

        if self.total_time >= self.target_time and self.phase == 2:
            self.angle = math.atan2(self.player.center_y - self.center_y, self.player.center_x - self.center_x)
            self.speed = MAX_SPEED
            self.phase += 1

        self.handle_movements()


