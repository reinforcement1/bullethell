import arcade
from ennemies import BasicEnnemyBehavior1, BasicEnnemyBehavior2, BasicEnnemyBehavior3
from ennemies.first_boss import FirstBoss
from player import Player
from constants import SCREEN_WIDTH


class Stage1:
    def __init__(self, ennemies_list: arcade.SpriteList, ennemies_bullet_list: arcade.SpriteList, player: Player):
        self.ennemies_list = ennemies_list
        self.ennemies_bullet_list = ennemies_bullet_list
        self.player = player

        self.actions = [
            {"time": 15.0, "action": self._spawn_boss},
            {"time": 7.5, "action": self._first_wave},
            {"time": 7.3, "action": self._spawn_left_side_ennemy, "kwargs": {"center_y": 550, "center_x": -105}},
            {"time": 7.1, "action": self._spawn_right_side_ennemy,
             "kwargs": {"center_y": 550, "center_x": SCREEN_WIDTH + 105}},
            {"time": 6.9, "action": self._spawn_left_side_ennemy, "kwargs": {"center_y": 600, "center_x": -110}},
            {"time": 6.7, "action": self._spawn_right_side_ennemy,
             "kwargs": {"center_y": 500, "center_x": SCREEN_WIDTH + 110}},
            {"time": 6.5, "action": self._spawn_left_side_ennemy, "kwargs": {"center_y": 500, "center_x": -120}},

            {"time": 4.3, "action": self._spawn_left_side_ennemy, "kwargs": {"center_y": 450, "center_x": -5}},
            {"time": 4.1, "action": self._spawn_right_side_ennemy,
             "kwargs": {"center_y": 450, "center_x": SCREEN_WIDTH + 5}},
            {"time": 3.9, "action": self._spawn_left_side_ennemy, "kwargs": {"center_y": 400, "center_x": -10}},
            {"time": 3.7, "action": self._spawn_right_side_ennemy,
             "kwargs": {"center_y": 400, "center_x": SCREEN_WIDTH + 10}},
            {"time": 3.5, "action": self._spawn_left_side_ennemy, "kwargs": {"center_y": 500, "center_x": -20}},
            {"time": 3.0, "action": self._first_wave},
        ]

        self.total_time = 0.0
        self.remainsEnnemies = True

    def _first_wave(self):
        for i in range(1, 5):
            ennemy = BasicEnnemyBehavior1(1.5, 100 + i * 80, 750, self.ennemies_bullet_list, self.player)
            self.ennemies_list.append(ennemy)
        for i in range(1, 4):
            ennemy = BasicEnnemyBehavior1(1.5, 140 + i * 80, 725, self.ennemies_bullet_list, self.player)
            self.ennemies_list.append(ennemy)

    def _spawn_left_side_ennemy(self, **kwargs):
        ennemy = BasicEnnemyBehavior2(1.5, kwargs.get("center_x"), kwargs.get("center_y"), self.ennemies_bullet_list,
                                      self.player)
        self.ennemies_list.append(ennemy)

    def _spawn_right_side_ennemy(self, **kwargs):
        ennemy = BasicEnnemyBehavior3(1.5, kwargs.get("center_x"), kwargs.get("center_y"), self.ennemies_bullet_list,
                                      self.player)
        self.ennemies_list.append(ennemy)

    def _spawn_boss(self):
        boss = FirstBoss(1.5, center_x=300, center_y=750, player=self.player, bullet_list=self.ennemies_bullet_list)
        self.ennemies_list.append(boss)

    def is_terminated(self):
        if not self.remainsEnnemies:
            if len(self.ennemies_list) == 0:
                return True
        return False

    def on_update(self, delta_time: float):
        self.total_time += delta_time

        if len(self.actions) == 0:
            self.remainsEnnemies = False
            return

        if self.total_time >= self.actions[-1]["time"]:
            item = self.actions.pop()
            if "kwargs" in item:
                item["action"](**item["kwargs"])
            else:
                item["action"]()
