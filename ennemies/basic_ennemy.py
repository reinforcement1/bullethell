import arcade

from constants import DECELERATION_RATE, MAX_SPEED
from player import Player
from bullets import TargetedBullet
from helpers import EntityCollisionsHelper


class BasicEnnemy(arcade.Sprite):
    def __init__(
        self,
        scale: float,
        center_x: float,
        center_y: float,
        bullet_list: arcade.SpriteList,
        texture: str = "sprites/basic_ennemy1.png",
    ):
        super().__init__(texture, scale)
        self.bullet_list = bullet_list

        self.center_y = center_y
        self.center_x = center_x

        self.life = 5

        self.speed = 0

        self.is_moving_up = False
        self.is_moving_down = False
        self.is_moving_left = False
        self.is_moving_right = False

        self.is_accelerating = False
        self.is_decelerating = False

    def take_damage(self):
        self.life -= 1

    def handle_movements(self):
        if self.is_decelerating:
            self.speed -= DECELERATION_RATE
            if self.speed <= 0:
                self.speed = 0
                self.is_decelerating = False

        if self.is_moving_up:
            self.center_y += self.speed
        if self.is_moving_down:
            self.center_y -= self.speed
        if self.is_moving_left:
            self.center_x -= self.speed
        if self.is_moving_right:
            self.center_x += self.speed

    def on_update(self, delta_time: float = 1 / 60):
        self.handle_movements()


class BasicEnnemyBehavior1(BasicEnnemy):
    def __init__(self, scale: float, center_x: float, center_y: float, bullet_list: arcade.SpriteList, player: Player):
        super().__init__(scale, center_x, center_y, bullet_list)

        self.player = player

        self.actions = [
            {"time": 5.0, "action": self._leave_screen},
            {"time": 2.5, "action": self._first_attack},
            {"time": 0.0, "action": self._enter_screen},
        ]
        self.total_time = 0.0

    def _enter_screen(self):
        self.is_moving_down = True
        self.is_decelerating = True
        self.speed = MAX_SPEED

    def _first_attack(self):
        b1 = TargetedBullet(
            scale=1.5,
            center_x=self.center_x - 45,
            center_y=self.center_y,
            target_x=self.player.center_x,
            target_y=self.player.center_y,
        )
        b2 = TargetedBullet(
            scale=1.5,
            center_x=self.center_x - 15,
            center_y=self.center_y,
            target_x=self.player.center_x,
            target_y=self.player.center_y,
        )
        b3 = TargetedBullet(
            scale=1.5,
            center_x=self.center_x + 15,
            center_y=self.center_y,
            target_x=self.player.center_x,
            target_y=self.player.center_y,
        )
        b4 = TargetedBullet(
            scale=1.5,
            center_x=self.center_x + 45,
            center_y=self.center_y,
            target_x=self.player.center_x,
            target_y=self.player.center_y,
        )

        self.bullet_list.append(b1)
        self.bullet_list.append(b2)
        self.bullet_list.append(b3)
        self.bullet_list.append(b4)

    def _leave_screen(self):
        self.is_moving_up = True
        self.is_moving_down = False
        self.speed = 4.0

    def on_update(self, delta_time: float = 1 / 60):
        super().on_update(delta_time)
        self.total_time += delta_time

        if len(self.actions) == 0:
            if EntityCollisionsHelper.is_out_of_bounds(self):
                self.remove_from_sprite_lists()
            return

        if self.total_time >= self.actions[-1]["time"]:
            self.actions.pop()["action"]()


class BasicEnnemyBehavior2(BasicEnnemyBehavior1):
    def __init__(self, scale: float, center_x: float, center_y: float, bullet_list: arcade.SpriteList, player: Player):
        super().__init__(scale, center_x, center_y, bullet_list, player)

    def _enter_screen(self):
        self.is_moving_right = True
        self.is_decelerating = True
        self.speed = MAX_SPEED

    def _leave_screen(self):
        self.is_moving_right = False
        self.is_moving_left = True
        self.speed = 4


class BasicEnnemyBehavior3(BasicEnnemyBehavior1):
    def __init__(self, scale: float, center_x: float, center_y: float, bullet_list: arcade.SpriteList, player: Player):
        super().__init__(scale, center_x, center_y, bullet_list, player)

    def _enter_screen(self):
        self.is_moving_left = True
        self.is_decelerating = True
        self.speed = MAX_SPEED

    def _leave_screen(self):
        self.is_moving_left = False
        self.is_moving_right = True
        self.speed = 4
