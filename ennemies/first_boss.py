import arcade

from constants import MAX_SPEED, DECELERATION_RATE
from player import Player
from bullets import BossBullet, TargetedBullet
from .basic_ennemy import BasicEnnemy


class FirstBoss(BasicEnnemy):
    def __init__(self, scale: float, center_x: float, center_y: float, bullet_list: arcade.SpriteList, player: Player):
        super().__init__(scale, center_x, center_y, bullet_list, texture="sprites/boss1.png")

        self.player = player
        self.life = 250
        self.total_time = 0.0
        self.loop_phase = False

        self.actions = [{"time": 0.0, "action": self._enter_screen}]
        self.loop = [
            {"time": 2.0, "action": self._first_attack},
            {"time": 5.0, "action": self._second_attack},
            {"time": 5.5, "action": self._second_attack},
            {"time": 8.0, "action": self._first_attack},
            {"time": 11.0, "action": self._third_attack},
            {"time": 11.5, "action": self._third_attack},
        ]

        self.loop_iterator = 0

    def _enter_screen(self):
        self.speed = MAX_SPEED
        self.is_moving_down = True
        self.is_decelerating = True

    def _move_right(self, **kwargs):
        self.is_moving_down = False
        self.is_moving_up = False
        self.is_moving_left = False
        self.is_moving_right = True
        self.is_decelerating = True
        self.speed = kwargs.get("speed", MAX_SPEED)

    def _move_left(self, **kwargs):
        self.is_moving_down = False
        self.is_moving_up = False
        self.is_moving_left = True
        self.is_moving_right = False
        self.is_decelerating = True
        self.speed = kwargs.get("speed", MAX_SPEED)

    def _first_attack(self):
        for i in range(0, 4):
            b = BossBullet(
                scale=1.5,
                center_x=self.center_x,
                center_y=self.center_y - 15,
                target_x=self.center_x - 50,
                target_y=self.center_y - 50,
                stop_time=0.3 + float(i) / 25.0,
                target_time=1.4 - float(i) / 5.0,
                player=self.player,
            )
            self.bullet_list.append(b)

        for i in range(0, 4):
            b = BossBullet(
                scale=1.5,
                center_x=self.center_x,
                center_y=self.center_y - 15,
                target_x=self.center_x - 25,
                target_y=self.center_y - 50,
                stop_time=0.3 + float(i) / 25.0,
                target_time=1.4 - float(i) / 5.0,
                player=self.player,
            )
            self.bullet_list.append(b)
        for i in range(0, 4):
            b = BossBullet(
                scale=1.5,
                center_x=self.center_x,
                center_y=self.center_y - 15,
                target_x=self.center_x,
                target_y=self.center_y - 50,
                stop_time=0.3 + float(i) / 25.0,
                target_time=1.4 - float(i) / 5.0,
                player=self.player,
            )
            self.bullet_list.append(b)
        for i in range(0, 4):
            b = BossBullet(
                scale=1.5,
                center_x=self.center_x,
                center_y=self.center_y - 15,
                target_x=self.center_x + 25,
                target_y=self.center_y - 50,
                stop_time=0.3 + float(i) / 25.0,
                target_time=1.4 - float(i) / 5.0,
                player=self.player,
            )
            self.bullet_list.append(b)
        for i in range(0, 4):
            b = BossBullet(
                scale=1.5,
                center_x=self.center_x,
                center_y=self.center_y - 15,
                target_x=self.center_x + 50,
                target_y=self.center_y - 50,
                stop_time=0.3 + float(i) / 25.0,
                target_time=1.4 - float(i) / 5.0,
                player=self.player,
            )
            self.bullet_list.append(b)

    def _second_attack(self):
        b1 = TargetedBullet(
            scale=1.5,
            center_x=self.center_x + 20,
            center_y=self.center_y - 15,
            target_x=self.player.center_x,
            target_y=self.player.center_y,
        )
        b2 = TargetedBullet(
            scale=1.5,
            center_x=self.center_x + 40,
            center_y=self.center_y,
            target_x=self.player.center_x,
            target_y=self.player.center_y,
        )
        b3 = TargetedBullet(
            scale=1.5,
            center_x=self.center_x + 60,
            center_y=self.center_y + 15,
            target_x=self.player.center_x,
            target_y=self.player.center_y,
        )
        b4 = TargetedBullet(
            scale=1.5,
            center_x=self.center_x + 80,
            center_y=self.center_y + 30,
            target_x=self.player.center_x,
            target_y=self.player.center_y,
        )

        self.bullet_list.append(b1)
        self.bullet_list.append(b2)
        self.bullet_list.append(b3)
        self.bullet_list.append(b4)

    def _third_attack(self):
        b1 = TargetedBullet(
            scale=1.5,
            center_x=self.center_x - 20,
            center_y=self.center_y - 15,
            target_x=self.player.center_x,
            target_y=self.player.center_y,
        )
        b2 = TargetedBullet(
            scale=1.5,
            center_x=self.center_x - 40,
            center_y=self.center_y,
            target_x=self.player.center_x,
            target_y=self.player.center_y,
        )
        b3 = TargetedBullet(
            scale=1.5,
            center_x=self.center_x - 60,
            center_y=self.center_y + 15,
            target_x=self.player.center_x,
            target_y=self.player.center_y,
        )
        b4 = TargetedBullet(
            scale=1.5,
            center_x=self.center_x - 80,
            center_y=self.center_y + 30,
            target_x=self.player.center_x,
            target_y=self.player.center_y,
        )

        self.bullet_list.append(b1)
        self.bullet_list.append(b2)
        self.bullet_list.append(b3)
        self.bullet_list.append(b4)

    def handle_movements(self):
        if self.is_decelerating:
            self.speed -= DECELERATION_RATE
            if self.speed <= 0:
                self.speed = 0.0
                self.is_decelerating = False

        if self.is_moving_up:
            self.center_y += self.speed
        if self.is_moving_down:
            self.center_y -= self.speed
        if self.is_moving_left:
            self.center_x -= self.speed
        if self.is_moving_right:
            self.center_x += self.speed

    def on_update(self, delta_time: float = 1 / 60):
        self.total_time += delta_time

        if self.loop_phase:
            if self.total_time >= self.loop[self.loop_iterator]["time"]:
                action = self.loop[self.loop_iterator]
                if "kwargs" in action:
                    action["action"](**action["kwargs"])
                else:
                    action["action"]()

                if self.loop_iterator == len(self.loop) - 1:
                    self.loop_iterator = 0
                    self.total_time = 0.0
                else:
                    self.loop_iterator += 1
        else:
            if self.total_time >= self.actions[-1]["time"]:
                self.actions.pop()["action"]()
            if len(self.actions) == 0:
                self.loop_phase = True

        self.handle_movements()
