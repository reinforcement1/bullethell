import arcade


class Explosion(arcade.Sprite):
    def __init__(self, center_x: float, center_y: float):
        super().__init__(filename="sprites/explosion1.png", scale=1, center_x=center_x, center_y=center_y)

    def update(self):
        self.alpha -= 25
        self.scale += 0.2
        if self.alpha <= 20:
            self.remove_from_sprite_lists()