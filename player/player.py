import arcade
from helpers import EntityCollisionsHelper

MOVEMENT_SPEED = 6
FIRE_COOLDOWN = 0.12
BULLET_SPEED = 18
LIFE_POINTS = 5

class Player(arcade.Sprite):
    def __init__(self, filename: str, scale: float, bullet_list: arcade.SpriteList, max_x: int, max_y: int):
        super().__init__(filename, scale)
        self.bullet_list = bullet_list
        self.is_moving_up = False
        self.is_moving_down = False
        self.is_moving_left = False
        self.is_moving_right = False

        self.life = LIFE_POINTS
        self.fire_cooldown = FIRE_COOLDOWN

        self.max_x = max_x
        self.max_y = max_y

    def go_left(self):
        self.is_moving_left = True

    def go_right(self):
        self.is_moving_right = True

    def go_up(self):
        self.is_moving_up = True

    def go_down(self):
        self.is_moving_down = True

    def stop_going_left(self):
        self.is_moving_left = False

    def stop_going_right(self):
        self.is_moving_right = False

    def stop_going_up(self):
        self.is_moving_up = False

    def stop_going_down(self):
        self.is_moving_down = False

    def stop_all_moves(self):
        self.stop_going_up()
        self.stop_going_down()
        self.stop_going_left()
        self.stop_going_right()

    def fire_projectile(self, x_offset: int = 0):
        projectile = arcade.Sprite("sprites/player_projectile.png", scale=1.5)

        projectile.center_x = self.center_x + x_offset
        projectile.bottom = self.top

        self.bullet_list.append(projectile)

    def handle_movements(self):
        if self.is_moving_up and EntityCollisionsHelper.can_go_up(self, MOVEMENT_SPEED, self.max_y):
            self.center_y += MOVEMENT_SPEED
        if self.is_moving_down and EntityCollisionsHelper.can_go_down(self, MOVEMENT_SPEED, 0):
            self.center_y -= MOVEMENT_SPEED
        if self.is_moving_left and EntityCollisionsHelper.can_go_left(self, MOVEMENT_SPEED, 0):
            self.center_x -= MOVEMENT_SPEED
        if self.is_moving_right and EntityCollisionsHelper.can_go_right(self, MOVEMENT_SPEED, self.max_x):
            self.center_x += MOVEMENT_SPEED

    def handle_bullets(self):
        for bullet in self.bullet_list:
            bullet.center_y += BULLET_SPEED
            if EntityCollisionsHelper.is_out_of_bounds(bullet):
                bullet.remove_from_sprite_lists()

    def take_damage(self):
        self.life -= 1

    def on_update(self, delta_time: float = 1/60):
        self.fire_cooldown -= delta_time
        if self.fire_cooldown < 0:
            self.fire_projectile()
            self.fire_projectile(-20)
            self.fire_projectile(20)
            self.fire_cooldown = FIRE_COOLDOWN

        self.handle_movements()
        self.handle_bullets()
