SCREEN_WIDTH = 600
SCREEN_HEIGHT = 700

# For Agent
LEFT, RIGHT, UP, DOWN, LEFT_UP, LEFT_DOWN, RIGHT_UP, RIGHT_DOWN, NOTHING = 0, 1, 2, 3, 4, 5, 6, 7, 8
ACTIONS = [LEFT, RIGHT, UP, DOWN, LEFT_UP, LEFT_DOWN, RIGHT_UP, RIGHT_DOWN]

REWARD_DEATH, REWARD_DAMAGE, REWARD_KILL, REWARD_HURT, REWARD_STUCK, REWARD_WIN = -10000, 50, 300, -5000, -2, 100

LEARNING_RATE = 0.1
DISCOUNT_FACTOR = 0.5

# Ennemies
MAX_SPEED = 10
DECELERATION_RATE = 0.3
