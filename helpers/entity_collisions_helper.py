import arcade
from constants import SCREEN_WIDTH, SCREEN_HEIGHT


class EntityCollisionsHelper:
    @staticmethod
    def can_go_up(sprite: arcade.Sprite, movement_speed: float, max_y: int):
        return sprite.top + movement_speed < max_y

    @staticmethod
    def can_go_down(sprite: arcade.Sprite, movement_speed: float, min_y: int):
        return sprite.bottom - movement_speed > min_y

    @staticmethod
    def can_go_left(sprite: arcade.Sprite, movement_speed: float, min_x: int):
        return sprite.left - movement_speed > min_x

    @staticmethod
    def can_go_right(sprite: arcade.Sprite, movement_speed: float, max_x: int):
        return sprite.right + movement_speed < max_x

    @staticmethod
    def is_out_of_bounds(sprite: arcade.Sprite):
        return sprite.top < 0 or sprite.bottom > SCREEN_HEIGHT or sprite.right < 0 or sprite.left > SCREEN_WIDTH

    @staticmethod
    def is_in_range(bullet: arcade.Sprite, min_x, max_x, min_y, max_y):
        if min_x < bullet.center_x < max_x and min_y < bullet.center_y < max_y:
            return True
        return False
