import arcade
from typing import List
from player import Player
from effects import Explosion
from ennemies import BasicEnnemy, FirstBoss
from stages import Stage1
from constants import SCREEN_WIDTH, SCREEN_HEIGHT, REWARD_DEATH, REWARD_HURT, REWARD_STUCK, REWARD_WIN, REWARD_DAMAGE, REWARD_KILL
from agent import Agent


class GameWindows(arcade.Window):
    def __init__(
            self, agent: Agent, width: int = 800, height: int = 600, title: str = "Arcade Window",
    ):
        super().__init__(width, height, title)

        self.player: Player = None
        self.player_list: arcade.SpriteList = None
        self.player_bullet_list: arcade.SpriteList = None
        self.ennemies_list: arcade.SpriteList = None
        self.ennemies_bullet_list: arcade.SpriteList = None
        self.explosions_list: arcade.SpriteList = None
        self.stage = None
        self.score = 0
        self.high_score = 0
        self.reward = None
        self.agent: Agent = agent

        self.generation = 0
        self.rewardCount = 0

    def setup(self, agent):
        self.generation += 1
        self.rewardCount = 0
        self.player_list = arcade.SpriteList()
        self.player_bullet_list = arcade.SpriteList()
        self.explosions_list = arcade.SpriteList()
        self.ennemies_list = arcade.SpriteList()
        self.ennemies_bullet_list = arcade.SpriteList()

        self.player = Player(
            "sprites/player.png",
            scale=1.5,
            bullet_list=self.player_bullet_list,
            max_x=SCREEN_WIDTH,
            max_y=SCREEN_HEIGHT,
        )
        self.player.center_x = SCREEN_WIDTH // 2
        self.player.center_y = 50

        self.player_list.append(self.player)

        self.stage = Stage1(self.ennemies_list, self.ennemies_bullet_list, self.player)
        self.high_score = self.score if self.score > self.high_score else self.high_score
        self.score = 0

        agent.player = self.player
        agent.bullet_list = self.ennemies_bullet_list
        agent.enemy_list = self.ennemies_list

        agent.evaluate_state()

    def game_over(self):
        self.setup(self.agent)

    def verify_win(self):
        if self.stage.is_terminated():
            self.score += self.player.life * 10
            self.reward = REWARD_WIN * self.score

    def on_key_press(self, symbol: int, modifiers: int):
        if symbol == arcade.key.Z:
            self.player.go_up()
        elif symbol == arcade.key.S:
            self.player.go_down()
        elif symbol == arcade.key.Q:
            self.player.go_left()
        elif symbol == arcade.key.D:
            self.player.go_right()

    def on_key_release(self, symbol: int, modifiers: int):
        if symbol == arcade.key.Z:
            self.player.stop_going_up()
        elif symbol == arcade.key.S:
            self.player.stop_going_down()
        elif symbol == arcade.key.Q:
            self.player.stop_going_left()
        elif symbol == arcade.key.D:
            self.player.stop_going_right()

    def on_draw(self):
        arcade.start_render()

        self.player_list.draw()
        self.player_bullet_list.draw()
        self.explosions_list.draw()
        self.ennemies_list.draw()
        self.ennemies_bullet_list.draw()
        self.draw_stats()

    def draw_stats(self):
        arcade.draw_text("Score : " + str(self.score), 10, 675, arcade.color.RED, 12)
        arcade.draw_text("Life : " + str(self.player.life), 10, 650, arcade.color.RED, 12)
        arcade.draw_text("High Score : " + str(self.high_score), 10, 625, arcade.color.RED, 12)
        arcade.draw_text("Reward : " + str(self.rewardCount), 10, 600, arcade.color.RED, 12)
        arcade.draw_text("Generation : " + str(self.generation), 10, 575, arcade.color.RED, 12)

    def handle_bullets_collisions(self):
        for bullet in self.player_bullet_list:
            hit_list: List[BasicEnnemy] = arcade.check_for_collision_with_list(bullet, self.ennemies_list)
            if len(hit_list) > 0:
                bullet.remove_from_sprite_lists()
            for ennemy in hit_list:
                ennemy.take_damage()
                self.reward = REWARD_DAMAGE
                if ennemy.life <= 0:
                    self.score += 100 if type(ennemy) is FirstBoss else 1
                    self.explosions_list.append(Explosion(ennemy.center_x, ennemy.center_y))
                    self.reward = REWARD_KILL
                    ennemy.remove_from_sprite_lists()

        hit_list: List[arcade.Sprite] = arcade.check_for_collision_with_list(self.player, self.ennemies_bullet_list)
        if len(hit_list) > 0:
            self.reward = REWARD_HURT
            self.player.take_damage()
            self.explosions_list.append(Explosion(self.player.center_x, self.player.center_y))
            if self.player.life <= 0:
                self.reward = REWARD_DEATH
        for bullet in hit_list:
            bullet.remove_from_sprite_lists()

    def handle_protagonists_collisions(self):
        hit_list: List[arcade.Sprite] = arcade.check_for_collision_with_list(self.player, self.ennemies_list)
        if len(hit_list) > 0:
            self.reward = REWARD_HURT
            self.player.take_damage()
            self.explosions_list.append(Explosion(self.player.center_x, self.player.center_y))
            if self.player.life <= 0:
                self.reward = REWARD_DEATH

    def on_update(self, delta_time: float):
        self.reward = REWARD_STUCK
        self.agent.perform_best_action()
        self.agent.evaluate_state()

        self.player_list.on_update(delta_time)
        self.ennemies_list.on_update(delta_time)
        self.ennemies_bullet_list.on_update(delta_time)
        self.explosions_list.update()

        self.handle_bullets_collisions()
        self.handle_protagonists_collisions()
        self.stage.on_update(delta_time)

        self.verify_win()

        self.agent.evaluate_state()
        self.rewardCount += self.reward
        self.agent.get_reward(self.reward)

        if self.stage.is_terminated() or self.player.life <= 0:
            self.setup(self.agent)


def main():
    agent = Agent()
    window = GameWindows(agent, SCREEN_WIDTH, SCREEN_HEIGHT, "BulletHell From Heaven")
    window.setup(agent)
    arcade.run()


if __name__ == "__main__":
    main()
