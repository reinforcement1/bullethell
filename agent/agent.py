from constants import (
    ACTIONS,
    LEFT,
    RIGHT,
    UP,
    DOWN,
    LEFT_UP,
    LEFT_DOWN,
    RIGHT_DOWN,
    RIGHT_UP,
    NOTHING,
    SCREEN_WIDTH,
    SCREEN_HEIGHT,
    LEARNING_RATE,
    DISCOUNT_FACTOR,
)
import itertools
import arcade
from helpers import EntityCollisionsHelper
from ennemies import FirstBoss


class Agent:
    def __init__(self):
        self.player = None
        self.detection_map = {}
        self.init_detection_map()
        self.bullet_list: arcade.SpriteList = None
        self.enemy_list: arcade.SpriteList = None
        self.state = None
        self.last_action = None
        self.learning_rate = LEARNING_RATE
        self.discount_factor = DISCOUNT_FACTOR
        self.previous_state = None

    def best_action(self):
        return max(
            self.detection_map[self.state].keys(),
            key=lambda k: self.detection_map[self.state][k],
        )

    def calculate_difficulty(self, nb_bullets):
        if type(nb_bullets) != int:
            return nb_bullets

        if nb_bullets == 0:
            return 0
        elif nb_bullets < 10:
            return 1
        return 2

    def apply_action(self, action):
        self.player.stop_all_moves()

        if action == LEFT:
            self.player.go_left()
        elif action == RIGHT:
            self.player.go_right()
        elif action == UP:
            self.player.go_up()
        elif action == DOWN:
            self.player.go_down()
        elif action == LEFT_UP:
            self.player.go_left()
            self.player.go_up()
        elif action == LEFT_DOWN:
            self.player.go_left()
            self.player.go_down()
        elif action == RIGHT_UP:
            self.player.go_right()
            self.player.go_up()
        elif action == RIGHT_DOWN:
            self.player.go_right()
            self.player.go_down()

    def perform_best_action(self):
        self.last_action = self.best_action()
        self.apply_action(self.last_action)

    def init_detection_map(self):
        for combination in itertools.product([0, 1, 2], repeat=11):
            self.detection_map[combination] = {
                LEFT: 0,
                RIGHT: 0,
                UP: 0,
                DOWN: 0,
                LEFT_UP: 0,
                LEFT_DOWN: 0,
                RIGHT_UP: 0,
                RIGHT_DOWN: 0,
                NOTHING: 0,
            }

    def update(self, previous_state, state, last_action, reward):
        # Q(st, at) = Q(st, at) + learning_rate * (reward + discount_factor * max(Q(state)) - Q(st, at))
        maxQ = max(self.detection_map[state].values())
        self.detection_map[previous_state][last_action] += self.learning_rate * (
            reward + self.discount_factor * maxQ - self.detection_map[previous_state][last_action]
        )

    def evaluate_state(self):
        dangerousity_list = [0] * 11

        for enemy in self.enemy_list:
            increase = 10 if type(enemy) is FirstBoss else 1
            if EntityCollisionsHelper.is_in_range(enemy, 0, self.player.left, 0, self.player.bottom):
                dangerousity_list[0] += increase
            elif EntityCollisionsHelper.is_in_range(enemy, self.player.left, self.player.right, 0, self.player.bottom):
                dangerousity_list[1] += increase
            elif EntityCollisionsHelper.is_in_range(enemy, self.player.right, SCREEN_WIDTH, 0, self.player.bottom):
                dangerousity_list[2] += increase
            elif EntityCollisionsHelper.is_in_range(
                enemy,
                self.player.right,
                SCREEN_WIDTH,
                self.player.bottom,
                self.player.top,
            ):
                dangerousity_list[3] += increase
            elif EntityCollisionsHelper.is_in_range(
                enemy,
                self.player.right,
                SCREEN_WIDTH,
                self.player.top,
                SCREEN_HEIGHT - 140,
            ):
                dangerousity_list[4] += increase
            elif EntityCollisionsHelper.is_in_range(
                enemy,
                self.player.left,
                self.player.right,
                self.player.top,
                SCREEN_HEIGHT - 140,
            ):
                dangerousity_list[5] += increase
            elif EntityCollisionsHelper.is_in_range(enemy, 0, self.player.left, self.player.top, SCREEN_HEIGHT - 140):
                dangerousity_list[6] += increase
            elif EntityCollisionsHelper.is_in_range(enemy, 0, self.player.left, self.player.bottom, self.player.top):
                dangerousity_list[7] += increase
            elif EntityCollisionsHelper.is_in_range(enemy, 0, self.player.left, SCREEN_HEIGHT - 140, SCREEN_HEIGHT):
                dangerousity_list[8] += increase
            elif EntityCollisionsHelper.is_in_range(
                enemy,
                self.player.left,
                self.player.right,
                SCREEN_HEIGHT - 140,
                SCREEN_HEIGHT,
            ):
                dangerousity_list[9] += increase
            elif EntityCollisionsHelper.is_in_range(
                enemy,
                self.player.right,
                SCREEN_WIDTH,
                SCREEN_HEIGHT - 140,
                SCREEN_HEIGHT,
            ):
                dangerousity_list[10] += increase
            # dangerousity_list[8] = True

        for bullet in self.bullet_list:
            if EntityCollisionsHelper.is_in_range(bullet, 0, self.player.left, 0, self.player.bottom):
                dangerousity_list[0] += 1
            elif EntityCollisionsHelper.is_in_range(bullet, self.player.left, self.player.right, 0, self.player.bottom):
                dangerousity_list[1] += 1
            elif EntityCollisionsHelper.is_in_range(bullet, self.player.right, SCREEN_WIDTH, 0, self.player.bottom):
                dangerousity_list[2] += 1
            elif EntityCollisionsHelper.is_in_range(
                bullet,
                self.player.right,
                SCREEN_WIDTH,
                self.player.bottom,
                self.player.top,
            ):
                dangerousity_list[3] += 1
            elif EntityCollisionsHelper.is_in_range(
                bullet,
                self.player.right,
                SCREEN_WIDTH,
                self.player.top,
                SCREEN_HEIGHT - 140,
            ):
                dangerousity_list[4] += 1
            elif EntityCollisionsHelper.is_in_range(
                bullet,
                self.player.left,
                self.player.right,
                self.player.top,
                SCREEN_HEIGHT - 140,
            ):
                dangerousity_list[5] += 1
            elif EntityCollisionsHelper.is_in_range(bullet, 0, self.player.left, self.player.top, SCREEN_HEIGHT - 140):
                dangerousity_list[6] += 1
            elif EntityCollisionsHelper.is_in_range(bullet, 0, self.player.left, self.player.bottom, self.player.top):
                dangerousity_list[7] += 1
            elif EntityCollisionsHelper.is_in_range(bullet, 0, self.player.left, SCREEN_HEIGHT - 140, SCREEN_HEIGHT):
                dangerousity_list[8] += 1
            elif EntityCollisionsHelper.is_in_range(
                bullet,
                self.player.left,
                self.player.right,
                SCREEN_HEIGHT - 140,
                SCREEN_HEIGHT,
            ):
                dangerousity_list[9] += 1
            elif EntityCollisionsHelper.is_in_range(
                bullet,
                self.player.right,
                SCREEN_WIDTH,
                SCREEN_HEIGHT - 140,
                SCREEN_HEIGHT,
            ):
                dangerousity_list[10] += 1

        self.previous_state = self.state
        self.state = tuple(map(self.calculate_difficulty, dangerousity_list))

    def get_reward(self, reward):
        self.update(self.previous_state, self.state, self.last_action, reward)
